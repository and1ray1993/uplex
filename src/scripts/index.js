window.addEventListener('DOMContentLoaded', () => {
  Vue.use(window.vuelidate.default);
  Vue.use(VueMask.VueMaskPlugin);

  const {
    required
  } = validators;
  const {
    email
  } = validators;

  new Vue({
    el: '#app',
    data: {
      tabIndex: 1,
      where: '',
      date: '',
      name: '',
      phone: '',
      email: '',
      social: '',
      conf: true,
      mask: '+###(##)###-##-##',
      src: false,
      video: false,
      isActive: false,
      title: ''
    },
    methods: {
      changeTabs(event) {
        this.tabIndex = +event.target.getAttribute('data-index');

        const tabs = document.querySelectorAll('.js-tab');
        tabs.forEach(item => {
          if (item.classList.contains('mod-help-active')) {
            item.classList.remove('mod-help-active');
          }
        });
        event.target.classList.add('mod-help-active');
      },
      getWhere(where, event) {
        document.querySelectorAll('.popup__checkbox img').forEach(item => {
          item.setAttribute('src', 'assets/img/popups/uncheck.svg');
        });
        if (event.target.getAttribute('src').includes('uncheck')) {
          this.where = where;
          event.target.setAttribute('src', 'assets/img/popups/checked.svg');
        } else if (event.target.getAttribute('src').includes('checked')) {
          event.target.setAttribute('src', 'assets/img/popups/uncheck.svg');
          this.where = '';
        }
      },
      sendForm() {
        this.title = document.title;
        $.ajax({
          url: 'send.php',
          type: 'POST',
          dataType: 'json',
          data: `param=${JSON.stringify(this._data)}`,
          success(msg) {
            if (msg === 'ok') {
              document.location.href = 'thank-you.html';
            } else {
              console.log('Ошибка');
            }
          },
        });

        this.where = '';
        this.date = '';
        this.name = '';
        this.phone = '';
        this.email = '';
        this.social = '';
        this.conf = true;
        this.title = '';

        document.location.href = '/thank-you.html';
      },
      addActive(event) {
        document.querySelectorAll('.request__check').forEach(item => {
          item.classList.remove('request__active');
        });
        event.target.classList.add('request__active');
        this.social = event.target.textContent;
      },
      showImage(event) {
        this.src = event.target.firstChild.getAttribute('src');
        document.body.style.overflow = 'hidden';
      },
      closeImage(event) {
        if (event.target.tagName !== 'IMG') {
          this.src = false;
          document.body.style.overflow = 'initial';
        }
      },
      closeVideo(event) {
        console.log(event.target);
        const div = $('.mod-video__play');
        if (!div.is(event.target) && div.has(event.target).length === 0) {
          $('.mod-video').hide();
          this.video = false;
        }
      },
    },
    validations: {
      email: {
        email,
        required,
      },
      phone: {
        required,
      },
      date: {
        required
      },
    }
  });

  $('.js-slider-spec').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    nextArrow: '.js-arrow-right-spec',
    prevArrow: '.js-arrow-left-spec',
    responsive: [
      {
        breakpoint: 961,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          variableHeight: true,
          variableWidth: true,
          nextArrow: '.js-arrow-right-spec',
          prevArrow: '.js-arrow-left-spec',
        },
      },
      {
        breakpoint: 370,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          nextArrow: '.js-arrow-right-spec',
          prevArrow: '.js-arrow-left-spec',
        },
      },
    ],
  });

  $('.js-slides-exp').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    adaptiveHeight: true,
    nextArrow: '.slides__right',
    prevArrow: '.slides__left',
    centerMode: true,
    responsive: [
      {
        breakpoint: 961,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          adaptiveHeight: true,
          nextArrow: '.slides__right',
          prevArrow: '.slides__left',
        },
      },
      {
        breakpoint: 370,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          adaptiveHeight: true,
          nextArrow: '.slides__right',
          prevArrow: '.slides__left',
        },
      },
    ],
  });

  if (window.outerWidth < 1140) {
    $('.js-slider-docs').slick({
      infinite: true,
      centerMode: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      variableHeight: true,
      variableWidth: true,
      nextArrow: '.js-arrow-left-exp',
      prevArrow: '.js-arrow-right-exp',
      responsive: [
        {
          breakpoint: 961,
          settings: {
            infinite: true,
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableHeight: true,
            variableWidth: true,
            nextArrow: '.js-arrow-left-exp',
            prevArrow: '.js-arrow-right-exp',
          },
        },
      ],
    });

    addOverlay();
    $('.experience-business__slider').click(() => {
      console.log('test');
      addOverlay();
    });

    function addOverlay() {
      $('.experience-business__img.slick-slide').each((i, item) => {
        $(item).addClass('mod-overlay');
      });
      $('.slick-center').removeClass('mod-overlay');
    }
  }

  function popup(classPopup) {
    const $popup = $(classPopup);
    const $closeBtnPopup = $('.js-close-popup');
    $popup.addClass('popup_active');
    $closeBtnPopup.click(() => $popup.removeClass('popup_active'));
  }

  $('.hero__button, .specialists__item_button, .breeding__dontknow').click(() => {
    popup('.mod-popup-2');
  });
  $('.header__button, .plan__button').click(() => {
    popup('.mod-popup-4');
  });
  $('.js-price__button').click(() => {
    popup('.mod-popup-3');
  });

  ymaps.ready(init);
  function init(){
    var myMap = new ymaps.Map("map", {
      center: [53.922170, 27.510062],
      zoom: 17
    }), myGeoObject = new ymaps.GeoObject({});
    myMap.geoObjects
      .add(myGeoObject)
      .add(new ymaps.Placemark([53.922020, 27.512297], {
      }, {
        preset: 'islands#icon',
        iconColor: '#b6221e'
      }));
    myMap.behaviors.disable('scrollZoom');
  }

  if (window.outerWidth >= 1140) {
    ymaps.ready(init);
    function init(){
      var myMap = new ymaps.Map("map", {
        center: [53.922170, 27.510062],
        zoom: 17
      }), myGeoObject = new ymaps.GeoObject({});
      myMap.geoObjects
        .add(myGeoObject)
        .add(new ymaps.Placemark([53.922020, 27.512297], {
        }, {
          preset: 'islands#icon',
          iconColor: '#b6221e'
        }));
      myMap.behaviors.disable('scrollZoom');
    }
  }
  if (window.outerWidth < 1140) {
    ymaps.ready(init);
    function init(){
      var myMap = new ymaps.Map("map", {
        center: [53.923643, 27.512268],
        zoom: 17
      }), myGeoObject = new ymaps.GeoObject({});
      myMap.geoObjects
        .add(myGeoObject)
        .add(new ymaps.Placemark([53.922020, 27.512297], {
        }, {
          preset: 'islands#icon',
          iconColor: '#b6221e'
        }));
      myMap.behaviors.disable('scrollZoom');
    }
  }
});
